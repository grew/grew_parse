<?php

return [
    'grew' => [
       'binary' => '/home/guillaum/.opam/last/bin/grew'
    ],

    'melt' => [
        'binary'  => '/usr/local/bin/MElt',
        'path'  =>  '/usr/local/bin/',
    ],

    'brown_to_json' => '/home/guillaum/www/parse/resources/ocaml/brown_to_json',

    'dep2pict' => [
        'binary' => '/home/guillaum/.opam/last/bin/dep2pict',
    ],

    'conll_tool' => [
        'binary' => '/home/guillaum/.opam/last/bin/conll_tool',
    ],

    'dot' => [
        'binary' => '/usr/bin/dot',
    ],

    'surf' => [
        'grs' => '/home/guillaum/resources/grew/POStoSSQ/grs/surf_synt_main.grs',
        'strat' => 'main',
    ],

    'mix' => [
        'grs' => '/home/guillaum/resources/grew/SSQtoDSQ/grs/main_dsq.grs',
        'strat' => 'main',
    ],

    'deep' => [
        'grs' => '/home/guillaum/resources/sequoia/sequoia_proj.grs',
        'strat' => 'deep',
    ],

    'dmrs' => [
        'grs' => '/home/guillaum/resources/grew/DSQtoDMRS/grs/dmrs_main.grs',
        'strat' => 'demo',
    ],

    'amr' => [
        'grs' => '/home/guillaum/resources/grew/DSQtoAMR/grs/amr_main.grs',
        'strat' => 'demo',
    ],

];
